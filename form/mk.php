<form id="dsio-form" class="diso-form" action="<?php echo home_url(get_option('dsio_form_url'))?>" method="post"  >
    <input type="hidden" name="dsio-language" value="<?= $lang ?>">
    <p>
        <select name="dsio-product" class="select" id="dsio-product" data-validation="required">
            <option value="">Изберете го Вашиот производ *</option>
            <?php

            $products = explode('|',$allproducts[$id-1]);
            foreach ($products as $product){
                echo "<option value=\"$product\">$product</option>";
            }
            ?>
        </select>
    </p>
    <p>
        <select name="dsio-country" class="select" id="dsio-country" data-validation="required">
            <option value="">Изберете земја на испорака *</option>
            <option value="Србија">Srbija</option>
            <option value="Црна Гора">Crna Gora</option>
            <option value="Босна и Херцеговина">Босна и Херцеговина</option>
            <option value="Хрватска">Хрватска</option>
            <option value="Македонија">Македонија</option>
            <option value="Словенија">Словенија</option>
        </select>
    </p>
    <p class="left">
        <input name="dsio-firstName" class="text_input" type="text" id="dsio-firstName" data-validation="required" value="" placeholder="Име:*">
    </p>
    <p class="right">
        <input name="dsio-lastName" class="text_input" type="text" id="dsio-lastName" data-validation="required" value="" placeholder="Презиме:*">
    </p>
    <p class="left">
        <input name="dsio-address" class="text_input" type="text" id="dsio-address" data-validation="required" value="" placeholder="Адреса:*">
    </p>
    <p class="right">
        <input name="dsio-city" class="text_input" type="text" id="dsio-city" data-validation="required" value="" placeholder="Град:*">
    </p>
    <p class="left">
        <input name="dsio-postalCode" class="text_input" type="text" id="dsio-postalCode" data-validation="number" value="" placeholder="Поштенски број:*">
    </p>
    <p class="right">
        <input name="dsio-phone" class="text_input" type="text" id="dsio-phone" data-validation="number" data-validation-ignore="+" value="" placeholder="Контакт телефон:*">
    </p>
    <p class="">
        <input name="dsio-email" class="text_input" type="text" id="dsio-email" data-validation="email" value="" placeholder="Емаил:*">
    </p>
    <p class="">
        <input type="submit" value="Нарачајте" class="button" name="instant-order">
    </p>
</form>