<form id="dsio-form" class="diso-form" action="<?php echo home_url(get_option('dsio_form_url'))?>" method="post"  >
    <input type="hidden" name="dsio-language" value="<?= $lang ?>">
    <p>
        <select name="dsio-product" class="select" id="dsio-product" data-validation="required">
            <option value="">Izberite izdelek:*</option>
            <?php

            $products = explode('|',$allproducts[$id-1]);
            foreach ($products as $product){
                echo "<option value=\"$product\">$product</option>";
            }
            ?>
        </select>
    </p>
    <p>
        <select name="dsio-country" class="select" id="dsio-country" data-validation="required">
            <option value="">Izaberite zemlju dostave:*</option>
            <option value="Srbija">Srbija</option>
            <option value="Crna Gora">Crna Gora</option>
            <option value="Bosna i Hercegovina">Bosna i Hercegovina</option>
            <option value="Hrvatska">Hrvatska</option>
            <option value="Makedonija">Makedonija</option>
            <option value="Slovenija">Slovenija</option>
        </select>
    </p>
    <p class="left">
        <input name="dsio-firstName" class="text_input" type="text" id="dsio-firstName" data-validation="required" value="" placeholder="Ime:*">
    </p>
    <p class="right">
        <input name="dsio-lastName" class="text_input" type="text" id="dsio-lastName" data-validation="required" value="" placeholder="Priimek:*">
    </p>
    <p class="left">
        <input name="dsio-address" class="text_input" type="text" id="dsio-address" data-validation="required" value="" placeholder="Naslov:*">
    </p>
    <p class="right">
        <input name="dsio-city" class="text_input" type="text" id="dsio-city" data-validation="required" value="" placeholder="Kraj:*">
    </p>
    <p class="left">
        <input name="dsio-postalCode" class="text_input" type="text" id="dsio-postalCode" data-validation="number" value="" placeholder="Poštna številka:*">
    </p>
    <p class="right">
        <input name="dsio-phone" class="text_input" type="text" id="dsio-phone" data-validation="number" data-validation-ignore="+" value="" placeholder="Kontaktna številka:*">
    </p>
    <p class="">
        <input name="dsio-email" class="text_input" type="text" id="dsio-email" data-validation="email" value="" placeholder="Email:*">
    </p>
    <p class="">
        <input type="submit" value="Naročite" class="button" name="instant-order">
    </p>
</form>