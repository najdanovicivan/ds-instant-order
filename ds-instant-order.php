<?php

/*
Plugin Name: DS Instant Order
Plugin URI: http://deployed.systems/
Description: Instant order form for Landing Pages
Version: 1.0
Author: Najdanovic Ivan
Author URI: http://deployed.systems/
License: Proprietary
*/


//Admin Menus
function dsio_add_admin_menus()
{
    add_menu_page(
        'Orders',
        'Orders',
        'manage_options',
        'dsio',
        null,
        'dashicons-cart',
        20
    );

    add_submenu_page(
        'dsio',
        'Options',
        'Options',
        'manage_options',
        'dsio_options',
        'dsio_options_page_html'
    );
}
add_action('admin_menu', 'dsio_add_admin_menus');


//Custom Post Type For Orders
function dsio_order_post_type()
{
    register_post_type('dsio_order',
        [
            'labels'      => [
                'name'                  => __('Orders'),
                'singular_name'         => __('Order'),
                'edit_item'             => __('View Order'),
                'not_found'             => __('No orders found'),
                'not_found_in_trash'    => __('No orders found in Trash'),
            ],
            'public'      => false,
            'has_archive' => true,
            'show_ui' => true,
            'show_in_menu' => 'dsio',
            'capabilities' => array(
                'edit_post'          => 'manage_options',
                'read_post'          => 'manage_options',
                'delete_post'        => 'manage_options',
                'edit_posts'         => 'manage_options',
                'edit_others_posts'  => 'manage_options',
                'delete_posts'       => 'manage_options',
                'publish_posts'      => 'do_not_allow',
                'read_private_posts' => 'manage_options',
                'create_posts' => 'do_not_allow', // false < WP 4.5, credit @Ewout
            ),
            'supports' => false,
            'register_meta_box_cb' => 'dsio_admin',
        ]
    );
}
add_action('init', 'dsio_order_post_type');

//List Display

// Add custom columns
add_filter( 'manage_dsio_order_posts_columns', 'dsio_order_posts_columns' );
function dsio_order_posts_columns( $columns ) {
    return array(
        'cb' => '<input type="checkbox" />',
        'id' => __('Order Id'),
        'customer' => __('Customer'),
        'email' => __('Email'),
        'address' =>__( 'Address'),
        'city' =>__('City'),
        'product' =>__('Product'),
        'date2' => __('Date')
    );
}

//Configure custom columns
add_action( 'manage_dsio_order_posts_custom_column' , 'dsio_order_custom_columns', 10, 2 );

function dsio_order_custom_columns( $column, $post_id ) {
    switch ( $column ) {
        case 'id':
            _e('Order '.$post_id);
            break;
        case 'customer':
            echo get_post_meta( $post_id, 'dsio_order_first_name', true ).' '.get_post_meta( $post_id, 'dsio_order_last_name', true );
            break;
        case 'email':
            echo get_post_meta( $post_id, 'dsio_order_email', true );
            break;
        case 'address':
            echo get_post_meta( $post_id, 'dsio_order_address', true );
            break;
        case 'product':
        	echo get_post_meta( $post_id, 'dsio_order_product', true );
            break;
        case 'city':
            echo get_post_meta( $post_id, 'dsio_order_city', true );
            break;
        case 'date2':
    		echo get_the_date('', $post_id).' '.get_the_time('', $post_id);
            break;
    }
}

//Single Order Display

//Remove Publishing
function dsio_remove_publish_box() {
    remove_meta_box( 'slugdiv', 'dsio_order', 'normal' );
    remove_meta_box( 'submitdiv', 'dsio_order', 'side' );
}
add_action( 'admin_menu', 'dsio_remove_publish_box' );

//Ad meta box
function dsio_admin() {
    add_meta_box( 'instant_order_meta_box',
        'Order Details',
        'display_order_details_meta_box',
        'dsio_order', 'normal', 'high'
    );
}

//Configure meta box
function display_order_details_meta_box( $dsorder) {


    $product = esc_html( get_post_meta( $dsorder->ID, 'dsio_order_product', true ) );
    $country = esc_html( get_post_meta( $dsorder->ID, 'dsio_order_country', true ) );
    $firstName = esc_html( get_post_meta( $dsorder->ID, 'dsio_order_first_name', true ) );
    $lastName = esc_html( get_post_meta( $dsorder->ID, 'dsio_order_last_name', true ) );
    $address = esc_html( get_post_meta( $dsorder->ID, 'dsio_order_address', true ));
    $city = esc_html( get_post_meta( $dsorder->ID, 'dsio_order_city', true ));
    $postalCode = esc_html( get_post_meta( $dsorder->ID, 'dsio_order_postal_code', true ));
    $phone = esc_html( get_post_meta( $dsorder->ID, 'dsio_order_phone', true ));
    $email = esc_html( get_post_meta( $dsorder->ID, 'dsio_order_email', true ));
    $lang = esc_html( get_post_meta( $dsorder->ID, 'dsio_order_language', true ));

    ?>
    <h1>Order ID: <?php echo $dsorder->ID ?></h1>
    <p>Date:  <?php echo $dsorder->post_date ?></p>

    <table class="widefat">
        <tbody>
        <tr>
            <td class="row-title"><label for="tablecell"><?php esc_attr_e(
                        'Date', 'wp_admin_style'
                    ); ?></label></td>
            <td><?php esc_attr_e( $dsorder->post_date, 'wp_admin_style' ); ?></td>
        </tr>
        <tr>
            <td class="row-title"><label for="tablecell"><?php esc_attr_e(
                        'Product', 'wp_admin_style'
                    ); ?></label></td>
            <td><?php esc_attr_e( $product, 'wp_admin_style' ); ?></td>
        </tr>
        <tr>
            <td class="row-title"><label for="tablecell"><?php esc_attr_e(
                        'Country', 'wp_admin_style'
                    ); ?></label></td>
            <td><?php esc_attr_e( $country, 'wp_admin_style' ); ?></td>
        </tr>
        <tr>
            <td class="row-title"><label for="tablecell"><?php esc_attr_e(
                        'First Name', 'wp_admin_style'
                    ); ?></label></td>
            <td><?php esc_attr_e( $firstName, 'wp_admin_style' ); ?></td>
        </tr>
        <tr>
            <td class="row-title"><label for="tablecell"><?php esc_attr_e(
                        'Email', 'wp_admin_style'
                    ); ?></label></td>
            <td><?php esc_attr_e( $email, 'wp_admin_style' ); ?></td>
        </tr>
        <tr>
            <td class="row-title"><label for="tablecell"><?php esc_attr_e(
                        'Last Name', 'wp_admin_style'
                    ); ?></label></td>
            <td><?php esc_attr_e( $lastName, 'wp_admin_style' ); ?></td>
        </tr>
        <tr>
            <td class="row-title"><label for="tablecell"><?php esc_attr_e(
                        'Address', 'wp_admin_style'
                    ); ?></label></td>
            <td><?php esc_attr_e( $address, 'wp_admin_style' ); ?></td>
        </tr>
        <tr>
            <td class="row-title"><label for="tablecell"><?php esc_attr_e(
                        'City', 'wp_admin_style'
                    ); ?></label></td>
            <td><?php esc_attr_e( $city, 'wp_admin_style' ); ?></td>
        </tr>
        <tr>
            <td class="row-title"><label for="tablecell"><?php esc_attr_e(
                        'Postal Code', 'wp_admin_style'
                    ); ?></label></td>
            <td><?php esc_attr_e( $postalCode, 'wp_admin_style' ); ?></td>
        </tr>
        <tr>
            <td class="row-title"><label for="tablecell"><?php esc_attr_e(
                        'Phone', 'wp_admin_style'
                    ); ?></label></td>
            <td><?php esc_attr_e( $phone, 'wp_admin_style' ); ?></td>
        </tr>
        <tr>
            <td class="row-title"><label for="tablecell"><?php esc_attr_e(
                        'Language', 'wp_admin_style'
                    ); ?></label></td>
            <td><?php esc_attr_e( $lang, 'wp_admin_style' ); ?></td>
        </tr>
        </tbody>
    </table>
    <?php
}

//Save data from custom mete boxes
add_action( 'save_post', 'add_order_fields', 10, 2 );
function add_order_fields( $order_id, $instant_order ) {
    // Check post type for movie reviews
    if ( $instant_order->post_type == 'dsio_order' ) {
        // Store data in post meta table if present in post data

        //if ( isset( $_POST['movie_review_rating'] ) && $_POST['movie_review_rating'] != '' ) {
        //   update_post_meta( $order_id, 'movie_rating', $_POST['movie_review_rating'] );
        // }

    }
}

//Options

//Options display
function dsio_options_page_html()
{


    if (!current_user_can('manage_options')) {
        return;
    }
    ?>
    <div class="wrap">
        <h1><?= esc_html(get_admin_page_title()); ?></h1>
        <form method="post">
            <?php
            if (isset($_POST['dsio_email'])){
                update_option('dsio_email',$_POST['dsio_email']);
            };
            if (isset($_POST['dsio_reply_name'])){
                update_option('dsio_reply_name',$_POST['dsio_reply_name']);
            };
            if (isset($_POST['dsio_reply_email'])){
                update_option('dsio_reply_email',$_POST['dsio_reply_email']);
            };
            if (isset($_POST['dsio_form_url'])){
                update_option('dsio_form_url',$_POST['dsio_form_url']);
            };
            if (isset($_POST['dsio_products'])){
                update_option('dsio_products',$_POST['dsio_products']);
            };

            $dsioemail=  get_option('dsio_email');

            $dsioreplyname = get_option('dsio_reply_name');
            $dsioreplyemail = get_option('dsio_reply_email');
            $dsioformurl=  get_option('dsio_form_url');
            $dsioproducts = get_option('dsio_products');

            echo "<table class='form-table'><tbody>";

            echo "<tr><th scope=\"row\"><label for=\"dsio_email\">Email for orders:</label></th>";
            echo "<td><input name=\"dsio_email\" type=\"text\" id=\"dsio_email\" value=\"$dsioemail\" class=\"regular-text\"></td></tr>";

            echo "<tr><th scope=\"row\"><label for=\"dsio_reply_email\">\"Reply To:\" Email Addres:</label></th>";
            echo "<td><input name=\"dsio_reply_email\" type=\"text\" id=\"dsio_reply_email\" value=\"$dsioreplyemail\" class=\"regular-text\"></td></tr>";


            echo "<tr><th scope=\"row\"><label for=\"dsio_reply_name\">\"Reply To:\" Name:</label></th>";
            echo "<td><input name=\"dsio_reply_name\" type=\"text\" id=\"dsio_reply_name\" value=\"$dsioreplyname\" class=\"regular-text\"></td></tr>";


            echo "<tr><th scope=\"row\"><label for=\"dsio_form_url\">Form relative url:</label></th>";
            echo "<td><input name=\"dsio_form_url\" type=\"text\" id=\"dsio_form_url\" value=\"$dsioformurl\" class=\"regular-text\"></td></tr>";


            echo "</tbody></table>";

            echo "<p><label for=\"dsio_products\">List of products: Product names with prices seperated with |. Groups separated with new line</label></p>";
            echo "<h2 class=\"title\">Products</h2>";
            echo "<textarea name='dsio_products' id='dsio_products' class=\"large-text code\" rows=\"3\">$dsioproducts</textarea>";
            // output security fields for
            // output save settings button

            submit_button("Save Settings");
            ?>
        </form>
    </div>
    <?php
}



//Front End

//Short Code
function dsio_shortcodes_init()
{
    function form_shortcode($atts = [], $content = null)
    {

        $attributes = shortcode_atts( array(
            'id' => 1,
            'lang' => 'rs'
        ), $atts );

        ob_start();

        html_form_code($attributes['id'],$attributes['lang']);

        return ob_get_clean();
    }

    add_shortcode('instant-order', 'form_shortcode');

    function write_order_shortcode($atts = [], $content = null)
    {

        ob_start();
        writeOrder();
        return ob_get_clean();
    }

    add_shortcode('write-order', 'write_order_shortcode');
}
add_action('init', 'dsio_shortcodes_init');


add_action( 'wp_enqueue_scripts', 'my_enqueue' );
function my_enqueue( $hook ) {
    wp_enqueue_script( 'jquerry-form-validator',
        plugins_url( '/js/jquery.form-validator.min.js', __FILE__ ),
        array( 'jquery' )
    );

    wp_enqueue_style( 'dsio-form',  plugins_url( '/css/form.css', __FILE__ ), array(), '1.1', 'all');

    wp_enqueue_script( 'dsio-validator',
        plugins_url( '/js/form-validation.js', __FILE__ ),
        array(),
        false,
        true
    );
}

function html_form_code($id,$lang) {

    $dsioproducts = get_option('dsio_products');
    $allproducts = explode("\n",$dsioproducts);
    if ($id < 1 || $id  > count($allproducts))
        echo "No products for Id:$id<br/>";
    else {
        $filename = dirname(__FILE__) . '/form/' . $lang . '.php';
        if (file_exists($filename))
            require_once($filename);
        else
            echo "No template for languge:$lang<br/>";
    }

}

function writeOrder() {

    // if the submit button is clicked, send the email
    if ( isset( $_POST['instant-order'] ) ) {

        // sanitize form values
        $product    = sanitize_text_field( $_POST["dsio-product"] );
        $country    = sanitize_text_field( $_POST["dsio-country"] );
        $firstName  = sanitize_text_field( $_POST["dsio-firstName"] );
        $lastName   = sanitize_text_field( $_POST["dsio-lastName"] );
        $address    = sanitize_text_field( $_POST["dsio-address"] );
        $city       = sanitize_text_field( $_POST["dsio-city"] );
        $postalCode = sanitize_text_field( $_POST["dsio-postalCode"] );
        $phone      = sanitize_text_field( $_POST["dsio-phone"] );
        $email      = sanitize_text_field( $_POST["dsio-email"] );
        $lang       = sanitize_text_field( $_POST["dsio-language"] );

        if (empty($product) || empty($country) || empty($email)|| empty($firstName) || empty($lastName) || empty($lastName) || empty($address) || empty($city) || empty($postalCode) || empty($phone))
            return;

        $post_id = wp_insert_post(array (
            'post_type' => 'dsio_order',
            'post_title' => 'Order From: '.$firstName.' '.$lastName,
        ));

        if ($post_id != 0){
            update_post_meta( $post_id, 'dsio_order_email', $email );
            update_post_meta( $post_id, 'dsio_order_product', $product );
            update_post_meta( $post_id, 'dsio_order_country', $country );
            update_post_meta( $post_id, 'dsio_order_first_name', $firstName );
            update_post_meta( $post_id, 'dsio_order_last_name', $lastName );
            update_post_meta( $post_id, 'dsio_order_address', $address );
            update_post_meta( $post_id, 'dsio_order_city', $city );
            update_post_meta( $post_id, 'dsio_order_postal_code', $postalCode );
            update_post_meta( $post_id, 'dsio_order_phone', $phone );
            update_post_meta( $post_id, 'dsio_order_language', $lang );

            emailOrder($post_id);
            if (!empty($email))
                emailCustomer($post_id);
        }



    }
}
function emailOrder($post_id){
    $product = esc_html( get_post_meta( $post_id, 'dsio_order_product', true ) );
    $country = esc_html( get_post_meta( $post_id, 'dsio_order_country', true ) );
    $firstName = esc_html( get_post_meta( $post_id, 'dsio_order_first_name', true ) );
    $lastName = esc_html( get_post_meta( $post_id, 'dsio_order_last_name', true ) );
    $address = esc_html( get_post_meta( $post_id, 'dsio_order_address', true ));
    $city = esc_html( get_post_meta( $post_id, 'dsio_order_city', true ));
    $postalCode = esc_html( get_post_meta( $post_id, 'dsio_order_postal_code', true ));
    $phone = esc_html( get_post_meta( $post_id, 'dsio_order_phone', true ));

    $emails = get_option('dsio_email');
    $title = 'Nova porudbina sa sajta '.get_bloginfo( 'name' ).' od '. $firstName .' '.$lastName;
    $message = "Podaci za isporuku";
    $message.= "\nIme: $firstName";
    $message.= "\nPrezime: $lastName";
    $message.= "\nAdresa: $address";
    $message.= "\nPostanski Broj: $postalCode";
    $message.= "\nGrad: $city";
    $message.= "\nDrzava: $country";
    $message.= "\nTelefon: $phone";
    $message.= "\n\nProizvod: $product";

    $headers = 'From: "'.get_bloginfo( 'name' ).'" <no-reply@'.$_SERVER['HTTP_HOST'].'>';
    wp_mail($emails,$title,$message,$headers);
}

function emailCustomer($post_id){
    $product = esc_html( get_post_meta( $post_id, 'dsio_order_product', true ) );
    $country = esc_html( get_post_meta( $post_id, 'dsio_order_country', true ) );
    $firstName = esc_html( get_post_meta( $post_id, 'dsio_order_first_name', true ) );
    $lastName = esc_html( get_post_meta( $post_id, 'dsio_order_last_name', true ) );
    $address = esc_html( get_post_meta( $post_id, 'dsio_order_address', true ));
    $city = esc_html( get_post_meta( $post_id, 'dsio_order_city', true ));
    $postalCode = esc_html( get_post_meta( $post_id, 'dsio_order_postal_code', true ));
    $phone = esc_html( get_post_meta( $post_id, 'dsio_order_phone', true ));
    $email = esc_html( get_post_meta( $post_id, 'dsio_order_email', true ));

    $reply_to_email = get_option('dsio_reply_email');
    $reply_to_name = get_option('dsio_reply_name');

    $title = 'Vaša porudbina sa sajta '.get_bloginfo( 'name' );
    $message = "Poštovani $firstName $lastName,";
    $message.= "\nPrimili smo Vašu narudžbinu i ona će bit obrađena u najskorijem mogućem roku. U nastavku su informacije o Vašoj narudžbini\n\n";

    $message.= "Adresa za isporuku";
    $message.= "\nIme: $firstName";
    $message.= "\nPrezime: $lastName";
    $message.= "\nAdresa: $address";
    $message.= "\nPostanski Broj: $postalCode";
    $message.= "\nGrad: $city";
    $message.= "\nDrzava: $country";
    $message.= "\nTelefon: $phone";
    $message.= "\n\nProizvod: $product";

    $headers = array(
        'From: "'.get_bloginfo( 'name' ).'" <no-reply@'.$_SERVER['HTTP_HOST'].'>',
        'Reply-To: '.$reply_to_name.' <'.$reply_to_email.'>',
    );

    wp_mail($email,$title,$message,$headers);
}
